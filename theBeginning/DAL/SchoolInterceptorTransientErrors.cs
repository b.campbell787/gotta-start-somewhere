﻿using System;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Infrastructure.Interception;
using System.Data.Entity.SqlServer;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Reflection;
using System.Linq;
using theBeginning.Logging;

namespace theBeginning.DAL
{

    /// <summary>
    /// The code overrides the ReaderExecuting method, which is called for queries that can return multiple rows of data.
    /// 
    /// If you wanted to check connection resiliency for other types of queries,m you could also override the NonQueryExecuting and ScalarExecuting methods as the logging interceptor does
    /// </summary>
    public class SchoolInterceptorTransientErrors : DbCommandInterceptor
    {

        private int _counter = 0;
        private ILogger _logger = new Logger();


        /// <summary>
        /// On the Student page if "Throw" is entered as search string, this code creates a dummy SQL database exception for error number 20 (a transient type)_
        /// Other transient numbers are 64, 233, 10053, 10054, 10060, 10928, 10929, 40197, 40501, and 40613 but are version specific to the database
        /// 
        /// The Code returns the exception to EF instead of running the query and passing back query results. 
        /// The transient exception is returned four times and then the code reverts to noral procedure of passing the query to the database.
        /// 
        /// The value in the search box will be in command.Parameters[0][1] [first][lastname]
        /// </summary>
        /// <param name="command"></param>
        /// <param name="interceptionContext"></param>
        public override void ReaderExecuting(DbCommand command, DbCommandInterceptionContext<DbDataReader> interceptionContext)
        {
            bool throwTransientErrors = false;

            if (command.Parameters.Count > 0 && command.Parameters[0].Value.ToString() == "%Throw%")
            {
                throwTransientErrors = true;
                command.Parameters[0].Value = "%an%";
                command.Parameters[1].Value = "%an%";
            }

            if (throwTransientErrors && _counter < 4)
            {
                _logger.Information("Returning transient error for command: {0}", command.CommandText);
                _counter++;
                interceptionContext.Exception = CreateDummySqlException();
            }

        }

        private SqlException CreateDummySqlException()
        {
            //The instance of SQL Server you attempted to connect to does not support encryption

            var sqlErrorNumber = 20;

            var sqlErrorCtor = typeof(SqlError).GetConstructors(BindingFlags.Instance | BindingFlags.NonPublic)
                               .Where(c => c.GetParameters().Count() == 7).Single();

            var sqlError = sqlErrorCtor.Invoke(new object[] { sqlErrorNumber, (byte)0, (byte)0, "", "", "", 1 });

            var errorCollection = Activator.CreateInstance(typeof(SqlErrorCollection), true);

            var addMethod = typeof(SqlErrorCollection).GetMethod("Add", BindingFlags.Instance | BindingFlags.NonPublic);
            addMethod.Invoke(errorCollection, new[] { sqlError });

            var sqlExceptionCtor = typeof(SqlException).GetConstructors(BindingFlags.Instance | BindingFlags.NonPublic)
                                   .Where(c => c.GetParameters().Count() == 4).Single();

            var sqlException = (SqlException)sqlExceptionCtor.Invoke(new object[] { "Dummy", errorCollection, null, Guid.NewGuid() });

            return sqlException;
        }
    }
}

