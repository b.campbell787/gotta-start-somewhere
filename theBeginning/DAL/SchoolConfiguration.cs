﻿using System.Data.Entity;
using System.Data.Entity.SqlServer;
using System.Data.Entity.Infrastructure.Interception;

namespace theBeginning.DAL
{
    public class SchoolConfiguration : DbConfiguration
    {

        public SchoolConfiguration() 
        {
            SetExecutionStrategy("System.Data.SqlClient", () => new SqlAzureExecutionStrategy());

            //Configures the execution policy
            //Interceptors are executed in the order of registration (what order DbInterception.Add method is called)
            DbInterception.Add(new SchoolInterceptorTransientErrors());
            DbInterception.Add(new SchoolInterceptorLogging());
        }

    }
}