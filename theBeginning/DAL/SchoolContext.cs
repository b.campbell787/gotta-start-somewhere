﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Linq;
using System.Web;
using theBeginning.Models;

namespace theBeginning.DAL
{
    public class SchoolContext : DbContext
    {

        
        public SchoolContext() : base("SchoolContext") //Connection string 
        {
        }

        /// <summary>
        /// Creates a DbSet 
        /// https://docs.microsoft.com/en-us/dotnet/api/system.data.entity.dbset?redirectedfrom=MSDN&view=entity-framework-6.2.0
        /// </summary>
        public DbSet<Student> Students { get; set; }
        public DbSet<Enrollment> Enrollments { get; set; }
        public DbSet <Course> Courses { get; set; }


        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            //prevents table names from being pluralized
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
        }


    }
}