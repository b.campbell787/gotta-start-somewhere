﻿namespace theBeginning.Models
{

    public enum Grade
    {
        A, B, C, D, F
    }

    public class Enrollment
    {
        public int EnrollmentID { get; set; } //primary key
        public int CourseID { get; set; } // foreign key - navigation property is Course, an Enrollment entity is associated with one Course
        public int StudentID { get; set; } //foreign key - associated with one Student entity
        public Grade? Grade { get; set; } 

        public virtual Course Course { get; set; }
        public virtual Student Student { get; set; }


    }
}