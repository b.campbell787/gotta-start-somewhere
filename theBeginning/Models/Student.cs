﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace theBeginning.Models
{
    public class Student
    {
        
        public int ID { get; set; } //primary key
        public string LastName { get; set; } 
        public string FirstMidName { get; set; }

        public DateTime EnrollmentDate { get; set; }

        //navigation property - hold other entities that are related to this entity
        //virtual - can be used for laxy loading
        //used for one-to-many or many-to-many: must be a list where entries can be added, updated and deleted
        public virtual ICollection<Enrollment> Enrollments { get; set; } 




    }
}